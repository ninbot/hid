/* Copyright 2020 Paco Arjonilla Garcia <pacoarjonilla@yahoo.es>
 *
 * This file is part of ninos.
 *
 * ninos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ninos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ninos.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "evdev.hh"

#include <filesystem>
#include <iostream>

namespace nin {

evdev:: evdev()
  : file_descriptor {-1}
  , device          {nullptr}
{ }

evdev:: evdev(char const* device_path)
{
    open(device_path);
}

evdev:: ~evdev()
{
    close();
    poller.sync();
    if (device)
        libevdev_free(device);
    ::close(file_descriptor);
}

void
evdev:: open(char const* device_path)
{
    file_descriptor = ::open(device_path, O_RDONLY | O_NONBLOCK);
    if (file_descriptor == -1)
        throw std::runtime_error("Device open error");

    // Clear input buffers
    char buffer[32];
    for (;;)
    {
        pollfd pfd_v = {file_descriptor, POLLIN, 0};
        int pollret = poll(&pfd_v, 1, 0);
        if (pollret)
            read(file_descriptor, buffer, 32);
        else
            break;
    }

    int error = libevdev_new_from_fd(file_descriptor, &device);
    if (error) {
        ::close(file_descriptor);
        throw std::runtime_error("evdev error: Path is not an input device?"
                 " Try with /dev/input/eventX");
    }

    fill_metadata_cache();
    fill_event_types_cache();
    fill_absinfos();

    poller.request_watch(file_descriptor, this);
    for (int i = 0; i < 11; i++)
        libevdev_kernel_set_led_value(device, i, LIBEVDEV_LED_ON);
}

void
evdev:: close()
{
    poller.request_drop(file_descriptor);
}

void
evdev:: fill_metadata_cache()
{
    char const* cch;
    int val;
    auto & mdc = metadata_cache;

    // Single '=' is correct
    if ((cch = libevdev_get_name(device)))          mdc.name              = cch;
    if ((cch = libevdev_get_phys(device)))          mdc.physical_location = cch;
    if ((cch = libevdev_get_uniq(device)))          mdc.unique_identifier = cch;
    if ((val = libevdev_get_id_product(device)))    mdc.ID.product        = val;
    if ((val = libevdev_get_id_vendor(device)))     mdc.ID.vendor         = val;
    if ((val = libevdev_get_id_bustype(device)))    mdc.ID.bustype        = val;
    if ((val = libevdev_get_id_version(device)))    mdc.ID.version        = val;
    if ((val = libevdev_get_driver_version(device)))mdc.driver_version    = val;
}


async_poll<evdev *>
evdev:: poller = { &evdev::event_callback };

void
evdev:: event_callback(evdev * device)
{
    while (libevdev_has_event_pending(*device))
    {
        input_event event;
        switch (libevdev_next_event(*device,
                    LIBEVDEV_READ_FLAG_NORMAL, &event))
        {
        case LIBEVDEV_READ_STATUS_SUCCESS:
            if (event.type != EV_SYN)
                device->on_event(event);
            break;
        case -EAGAIN:
            std::cerr << "evdev: Again " << "\n";
            break;
        case LIBEVDEV_READ_STATUS_SYNC:
            std::cerr << "evdev: SYNCERROR " << "\n";
            while (libevdev_next_event(*device,
                        LIBEVDEV_READ_FLAG_SYNC, &event)
                    == LIBEVDEV_READ_STATUS_SYNC)
            { }
            break;
        default:
            std::cerr << "evdev: Unknown event" << "\n";
            throw;
        }
    }
}


void
evdev:: fill_event_types_cache()
{
    auto & etc = event_types_cache;

    for (unsigned int type = 0; type < EV_CNT; type++)
    if (libevdev_has_event_type(device, type))
    {
        char const* type_name = libevdev_event_type_get_name(type);
        etc[type] = { type_name ? type_name : "<UNKNOWN>", {} };

        unsigned int code_cnt;
        switch (type) {
            //case EV_SYN: code_cnt = SYN_CNT; break;
            case EV_KEY: code_cnt = KEY_CNT; break;
            case EV_REL: code_cnt = REL_CNT; break;
            case EV_ABS: code_cnt = ABS_CNT; break;
            case EV_MSC: code_cnt = MSC_CNT; break;
            case EV_SW : code_cnt =  SW_CNT; break;
            case EV_LED: code_cnt = LED_CNT; break;
            case EV_SND: code_cnt = SND_CNT; break;
            case EV_REP: code_cnt = REP_CNT; break;
            case EV_FF : code_cnt =  FF_CNT; break;
            default: code_cnt = 0; break;
        }

        for (unsigned int code = 0; code < code_cnt; code++)
        if (libevdev_has_event_code(device, type, code))
        {
            char const* code_name = libevdev_event_code_get_name(type,code);
            switch (code) {
            case BTN_A: code_name = "BTN_A"; break;
            case BTN_B: code_name = "BTN_B"; break;
            case BTN_X: code_name = "BTN_X"; break;
            case BTN_Y: code_name = "BTN_Y"; break;
            default: break;
            }
            etc[type].codes[code] = (code_name ? code_name : "<UNKNOWN>");
        }
    }
}

void
evdev:: fill_absinfos()
{
    if (event_types().contains(EV_ABS))
    for (auto const& code_element : event_types().at(EV_ABS).codes)
    {
        unsigned int code = code_element.first;
        ::input_absinfo const* raw_info = libevdev_get_abs_info(device, code);
        libevdev_enable_event_code(device, EV_ABS, code, raw_info);
        abs_infos_cache[code] = {
            raw_info->minimum,
            raw_info->maximum,
            raw_info->fuzz,
            raw_info->flat,
            raw_info->resolution };
    }
}

double
evdev:: normalize_value(input_event const& event)
{
    if (event.type != EV_ABS)
        return event.value;

    evdev_absinfo_t const& info = absinfo(event.code);
    if (std::abs(event.value) <= info.flat)
        return 0;
    if (info.minimum < 0 && event.value < 0)
        return double(event.value) / double(-info.minimum);
    if (info.maximum > 0 && event.value > 0)
        return double(event.value) / double(info.maximum);
    return event.value;
}



std::vector<std::unique_ptr<evdev>>
evdev_scan_all()
{
    std::vector<std::unique_ptr<evdev>> retvec;

    for (auto const& input_file :
            std::filesystem::directory_iterator("/dev/input") )
    {
        std::string filename = input_file.path().filename().string();
        if ( !filename.starts_with("event") )
            continue;

        try {
            retvec.push_back(std::make_unique<evdev>(input_file.path().c_str()));
            std::cerr
                << "Scan found a device at " << input_file << ": "
                << retvec.back()->metadata().name
                << "\n";
        }
        catch (std::exception const& error)
        {
            std::cerr
                << "Scan error at" << input_file << ": "
                << error.what() << "\n";
        }
    }
    return retvec;
}



} // namespace nin

#ifdef TEST

class evdev_debug : public nin::evdev
{
public:
    using nin::evdev::evdev;
    void on_event(input_event const& event) override
    {
        if (event.type == EV_MSC) return;
        std::cout
            << "time " << event.time.tv_sec << "." <<
               std::setw(6) << std::setfill('0') << event.time.tv_usec
            << " event " << name
            << " type " << event_types().at(event.type).name
            << " code " << event.code << "=" <<
               event_types().at(event.type).codes.at(event.code)
            << " value " << normalize_value(event)
            << "\n";
    }
    std::string name;
};

int main(int argc, char ** argv)
{
    {
        auto scan = nin::evdev_scan_all();
        std::cerr << "main: size " << scan.size() << "\n"; 
        for (auto & dev : scan) dev->close();
    }

    evdev_debug joycon {argv[1]};
    joycon.name = "argv[1]";
    std::cerr << "Input device found. "
        << "Name: " << joycon.metadata().name << "\n";
    for (auto const& t : joycon.event_types()) {
        std::cerr << "*** Type " << t.first << ": " << t.second.name << "\n";
        for (auto const& c : t.second.codes) {
            std::cerr << "  Code " << c.first << ": " << c.second << "\n";
            if (t.first == EV_ABS) {
                nin::evdev::evdev_absinfo_t info = joycon.absinfo(c.first);
                std::cerr << "    min        " << info.minimum << "\n";
                std::cerr << "    max        " << info.maximum << "\n";
                std::cerr << "    fuzz       " << info.fuzz << "\n";
                std::cerr << "    flat       " << info.flat << "\n";
                std::cerr << "    resolution " << info.resolution << "\n";
            }
        }
    }

    while(1)
    sleep(8);
}

#endif
